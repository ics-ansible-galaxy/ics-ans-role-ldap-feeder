class FilterModule(object):
    def filters(self):
        return {
            'csentry_to_ldap_feeder': self.csentry_to_ldap_feeder}

    def csentry_to_ldap_feeder(self, my_groups):
        ret = {}
        ret['nisnetgroups'] = {}
        for group, hosts in my_groups.items():
            ret['nisnetgroups'][group] = hosts

        ret['sudoers'] = []

        return ret
