# ics-ans-role-ldap-feeder

Ansible role to install ldap-feeder.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ldap-feeder
```

## License

BSD 2-clause
